//
//  Created by gryphonet on 1/27/14.
//
#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface AppRightPlugin : CDVPlugin

-(void)start:(CDVInvokedUrlCommand*)command;
-(void)reportBug:(CDVInvokedUrlCommand*)command;
-(void)reportException: (CDVInvokedUrlCommand*)command;
-(void)sendNow: (CDVInvokedUrlCommand*)command;
-(void)addMarker: (CDVInvokedUrlCommand*)command;
-(void)reportJsError: (CDVInvokedUrlCommand*)command;
-(void)init:(CDVInvokedUrlCommand*)command;

-(void)setUserClass:(CDVInvokedUrlCommand*)command;
-(void)clearUserClass:(CDVInvokedUrlCommand*)command;
-(void)startZone:(CDVInvokedUrlCommand*)command;
-(void)stopZone:(CDVInvokedUrlCommand*)command;
-(void)putUserMetadata:(CDVInvokedUrlCommand*)command;
-(void)removeUserMetadata:(CDVInvokedUrlCommand*)command;
-(void)clearUserMetadata:(CDVInvokedUrlCommand*)command;
-(void)reportTimer:(CDVInvokedUrlCommand*)command;
-(void)addAutoMarker:(CDVInvokedUrlCommand*)command;
-(void)reportAngularError:(CDVInvokedUrlCommand*)command;
@end
