//
//  Created by gryphonet on 1/27/14.
//

#import "AppRightPlugin.h"

#import <AppRight/AppRight.h>

#define TAG @"AppRight"

@implementation AppRightPlugin
-(void)init:(CDVInvokedUrlCommand *)command{
    [super pluginInitialize];
}

-(void)start:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionStart];
}
-(void)reportBug:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionReprotBug];
}
-(void)reportException:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionReportException];
}
-(void)sendNow:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionSendNow];
}
-(void)addMarker:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionAddMarker];
}
-(void)reportJsError:(CDVInvokedUrlCommand *)command{
    [self performCommand:command withJsAction:JSActionReportJsError];
}

-(void)setUserClass:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionSetUserClass];
}
-(void)clearUserClass:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionClearUserClass];
}
-(void)startZone:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionStartZone];
}
-(void)stopZone:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionStopZone];
}
-(void)putUserMetadata:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionPutUserMetadata];
}
-(void)removeUserMetadata:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionClearUserMetadata];
}
-(void)clearUserMetadata:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionClearUserMetadata];
}
-(void)reportTimer:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionReportTimer];
}
-(void)addAutoMarker:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionAddAutoMarker];
}
-(void)reportAngularError:(CDVInvokedUrlCommand*)command
{
    [self performCommand:command withJsAction:JSActionReportAngularError];
}


-(void)performCommand:(CDVInvokedUrlCommand*)command withJsAction:(JSAction)action{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        BOOL actionSucceeded = [AppRight handleFromJS:action withEventArgs:command.arguments];
        if(actionSucceeded) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"JS command preformed successfully."];
        }
        else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"JS command FAILED."];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}
@end
