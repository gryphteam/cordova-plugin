(function() {
	var iOSUserAgents = ['ipad', 'iphone', 'ipod'];

	module.exports = {
		sendNow: function() {
			inner().sendNow();
		},

		start: function(accountId, lifecyclePhase, opts) {
				opts = opts || {};
				var useExternalStorage = opts.useExternalStorage || true;
				var virtualAppName = opts.virtualAppName || '';
				inner().start(accountId, lifecyclePhase, useExternalStorage, virtualAppName);
		},

		reportException: function(exception) {
			inner().reportException(exception.message,exception.stack, exception.name);
		},

		reportBug: function(msg) {
	        var stack = null;
	        try{
	            throw new Error('');
	        }catch(e){
	            stack = e.stack;
			}

			inner().reportBug(msg, stack);
		},

		addMarker: function(msg) {
	        var stack = null;
	        try{
	            throw new Error('');
	        }catch(e){
	            stack = e.stack;
	        }

			inner().addMarker(msg, stack);
		},

		putUserMetadata: function(key, value) {
			inner().putUserMetadata(key, value);
		},

		removeUserMetadata: function(key) {
			inner().removeUserMetadata(key);
		},

		setUserClass: function(userclass) {
			inner().setUserClass(userclass);
		},

		clearUserClass: function() {
			inner().clearUserClass();
		},

		clearUserMetadata: function() {
			inner().clearUserMetadata();
		},

		startZone: function(zoneName) {
			inner().startZone(zoneName);
		},

		stopZone: function() {
			inner().stopZone();
		},

		startTimer: function(timerName, timeout) {
			timers.startTimer(timerName, timeout);
		},

		stopTimer: function(timerName) {
			timers.stopTimer(timerName, null);
		},

    	onAngularError: function(msg, stack) {
      		inner().onAngularError(msg, stack);
    	}
	};

	var timerKey = function(timerName) {
		return "appright.timers." + timerName;
	};

	var currentTime = function() {
		return new Date().getTime();
	};

	var timers = {
		startTimer: function(timerName, timeout) {
			inner().addAutoMarker("Timer " + timerName + " started")
			sessionStorage.setItem(timerKey(timerName), JSON.stringify({timeout: timeout, startTime: currentTime()}));
		},

		stopTimer: function(timerName, url) {
			var timer = JSON.parse(sessionStorage.getItem(timerKey(timerName)));

			if (timer) {
				var elapsed = currentTime() - timer.startTime;
				if (timer.timeout < elapsed) {
					inner().reportTimer(timerName, elapsed, timer.timeout, url);
				}

				sessionStorage.removeItem(timerKey(timerName));

				inner().addAutoMarker("Timer " + timerName + " stopped")
			}
		}
	};

	var isAndroidDevice = function() {
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			return true;
		}
		else{
			return false;
		}
	};

	var isAppRightJsDefined = function(){
		if (typeof AppRightJS !== 'undefined')
			return true;
		return false;
	}

	function checkIfiOS(userAgent) {
	  	for(var i = 0; i < iOSUserAgents.length; i++){
	  		if(userAgent.indexOf(iOSUserAgents[i]) > -1){
	            return true;
			}
		}
		return false;
	};

	var isiOSDevice = function() {
		var ua = navigator.userAgent.toLowerCase();
		var isiOS= checkIfiOS(ua);
		if(isiOS) {
			return true;
		}
		else{
			return false;
		}
	};

	var isCordova = function() {
		return typeof cordova != "undefined";
	};

	var isCordovaSupported = function() {
		return isCordova() && (isAndroidDevice() || isiOSDevice());
	};

	var logExceptions = function(o) {
		for(var property in AppRightJSUnsupported) {
			if (typeof AppRightJSUnsupported[property] == 'function') {
				(function(property) {
					var orig = o[property];
					o[property] = function() {
						try {
							orig.apply(o, arguments);
						} catch(e) {
							console.log("Failed to invoke AppRight inner function " + property + " with error " + e);
						}
					};
				})(property);
			}
		}
	};

	var innerObject;
	var inner = function() {
		if (innerObject) return innerObject;

		if (isCordovaSupported()) {
			innerObject = AppRightJSCordova;
		} else if (isAndroidDevice() && isAppRightJsDefined()) {
			innerObject = AppRightJS;
			innerObject.onAngularError = innerObject.onError;
		} else if (isiOSDevice()) {
			innerObject = AppRightJSiOS;
		} else {
			innerObject = AppRightJSUnsupported;
		}

		logExceptions(innerObject);

		return innerObject;
	};

	window.onerror = function( msg, url, line, col, error) {
	    var stack = null;
	    if(error)
	        stack = error.stack;

	    inner().onError(msg, url, line, stack);
	};

	document.addEventListener("click", function(event) {
		if (event.target.tagName === "BUTTON" || event.target.tagName === "INPUT"){
			var message = event.target.tagName + " clicked";
			if(event.target.type)
				message = message + "; type: "+ event.target.type;
			if(event.target.name)
				message =  message + "; name: " + event.target.name;
		 	if(event.target.id)
		    	message =  message + "; id: " + event.target.id;

		    inner().addAutoMarker(message);
		}
	}, true);

	window.addEventListener('load', function() {
		if(isCordovaSupported()){
			if(sessionStorage.getItem("appright.isCordovaReady") === "true"){
				inner().addAutoMarker("URL loaded: " + document.documentURI);
				sessionStorage.setItem("appright.urlMarkerAdded", "true");
			}
		}else
			inner().addAutoMarker("URL loaded: " + document.documentURI);
	});

	var AppRightJSiOS = {
		sendNow: function() {
	        AppRightJSiOS.handleAction('sendNow', null);
		},

		reportException: function(message, stack, exceptionName) {
	        var params = new Array(message, stack, exceptionName);
	        AppRightJSiOS.handleAction('reportException', params);
		},

		reportBug: function(msg, stack) {
	        var params = new Array(msg, stack);
	        AppRightJSiOS.handleAction('reportBug', params);
		},

		addMarker: function(msg, stack) {
	        var params = new Array(msg, stack);
	        AppRightJSiOS.handleAction('addMarker', params);
		},

		addAutoMarker: function(msg) {
	        var params = new Array(msg);
	        AppRightJSiOS.handleAction('addAutoMarker', params);
		},

		putUserMetadata: function(key, value) {
	        var params = new Array(key, value);
	        AppRightJSiOS.handleAction('putUserMetadata', params);
		},

		removeUserMetadata: function(key) {
	        var params = new Array(key);
	        AppRightJSiOS.handleAction('removeUserMetadata', params);
		},

		setUserClass: function(userclass) {
	        var params = new Array(userclass);
	        AppRightJSiOS.handleAction('setUserClass', params);
		},

		clearUserClass: function() {
	        AppRightJSiOS.handleAction('clearUserClass', null);
		},

		clearUserMetadata: function() {
	        var params = new Array(userclass);
	        AppRightJSiOS.handleAction('clearUserMetadata', null);
		},

		startZone: function(zoneName) {
	        var params = new Array(zoneName);
	        AppRightJSiOS.handleAction('startZone', params);
		},

		stopZone: function() {
	        AppRightJSiOS.handleAction('stopZone', null);
		},

		onError: function(msg, url, line, stack) {
	        var params = new Array(msg, url, line);
	        if(stack != null && stack != undefined){
	            params.push(stack);
	        }
	        AppRightJSiOS.handleAction('onError', params);
		},

		onAngularError: function(msg, stack) {
			var params = new Array(msg, stack);
			AppRightJSiOS.handleAction('onAngularError', params);
		},

		reportTimer: function(timerName, elapsed, timeout, url) {
			var params = new Array(timerName, elapsed, timeout, url);
	    AppRightJSiOS.handleAction('reportTimer', params);
		},

		handleAction: function(actionName, paramsArray) {
	        if(isiOSDevice() == false)
	            return;

	        if(actionName == null || actionName == undefined)
	            return;

	        if(paramsArray == null || paramsArray == undefined)
	            paramsArray = new Array('');

	        var params = new Array();
			for (var i = 0; i < paramsArray.length; i++) {
	            var param = paramsArray[i];
	            if(param == null || param == undefined)
	                param = "";

				params.push(param);
			}

	        var eventForSending = {
	            'action': actionName,
	            'params': params
	        };

	        var json = JSON.stringify(eventForSending);
	        AppRightJSiOS.sendToNative(json);
		},

	    sendToNative: function(json) {
	        var newUrl = 'app-right-js:^' + encodeURIComponent(json);

            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", newUrl);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
	    }
	};

	var cordovaSuccess = function() {};
	var cordovaFailure = function() {};

	if (isCordovaSupported()) {
		function onDeviceReady() {
			cordova.exec(AppRight.success,AppRight.failure,"AppRightPlugin","init",[""]);
			sessionStorage.setItem("appright.isCordovaReady", "true");
			if(sessionStorage.getItem("appright.urlMarkerAdded") !== "true"){
				inner().addAutoMarker("URL loaded: " + document.documentURI);
			}
		}
		document.addEventListener("deviceready", onDeviceReady, false);
	}

	var AppRightJSCordova = {
		sendNow: function() {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","sendNow",[]);
		},

		start: function(accountId, lifecyclePhase, useExternalStorage, virtualAppName) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","start",[accountId, lifecyclePhase, useExternalStorage, virtualAppName]);
		},

		reportException: function(message, stack, exceptionName) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","reportException",[message, stack, exceptionName]);
		},

		reportBug: function(msg, stack) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","reportBug",[msg, stack]);
		},

		addMarker: function(msg, stack) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","addMarker",[msg, stack]);
		},

		addAutoMarker: function(msg) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","addAutoMarker",[msg]);
		},

		putUserMetadata: function(key, value) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","putUserMetadata",[key, value]);
		},

		removeUserMetadata: function(key) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","removeUserMetadata",[key]);
		},

		setUserClass: function(userclass) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","setUserClass",[userclass]);
		},

		clearUserClass: function() {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","clearUserClass",[]);
		},

		clearUserMetadata: function() {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","clearUserMetadata",[]);
		},

		startZone: function(zoneName) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","startZone",[zoneName]);
		},

		stopZone: function() {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","stopZone",[]);
		},

		reportTimer: function(timerName, elapsed, timeout, url) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","reportTimer",[timerName, elapsed, timeout, url]);
		},

		onError: function(msg, url, line, stack) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","reportJsError",[msg,url,line,stack]);
		},

		onAngularError: function(msg, stack) {
			cordova.exec(cordovaSuccess,cordovaFailure,"AppRightPlugin","reportAngularError",[msg,stack]);
		}
	};

	var AppRightJSUnsupported = {
		sendNow: function() {
		},

		start: function(accountId, lifecyclePhase, useExternalStorage, virtualAppName) {
		},

		reportException: function(message, stack, exceptionName) {
		},

		reportBug: function(msg, stack) {
		},

		addMarker: function(msg, stack) {
		},

		addAutoMarker: function(msg) {
		},

		putUserMetadata: function(key, value) {
		},

		removeUserMetadata: function(key) {
		},

		setUserClass: function(userclass) {
		},

		clearUserClass: function() {
		},

		clearUserMetadata: function() {
		},

		startZone: function(zoneName) {
		},

		stopZone: function() {
		},

		reportTimer: function(timerName, elapsed, timeout, url) {
		},

		onError: function(msg, url, line, stack) {
		},

		onAngularError: function(msg, url){
		}
	}
})();
